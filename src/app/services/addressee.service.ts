import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of } from 'rxjs';

import { environment } from '../../environments/environment';
import { Addressee } from '../interfaces/addressee.interface';
import { getHeaderWithToken } from './utils';

@Injectable({
  providedIn: 'root'
})
export class AddresseService {
  private baseUrl: string = environment.url;

  constructor(private httpClient: HttpClient) { }

  addAddresse(addressee:Addressee){
    const url = `${this.baseUrl}/account/addAddressee`;

    return this.httpClient.post<any>(url, {addressee}, {headers:getHeaderWithToken()})
  }

  getAddressee(rutAddressee:string){
    const url = `${this.baseUrl}/account/getAddressee`;

    return this.httpClient.get(url, {headers:getHeaderWithToken(), params: {rutAddressee}})
      .pipe(
        catchError(err => of(err.error))
      );
  }

}
