import { HttpHeaders } from "@angular/common/http";

export const getHeaderWithToken = () => {
    return new HttpHeaders()
    .set('x-token', localStorage.getItem('token') || '');
}