import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, tap } from 'rxjs';

import { environment } from '../../environments/environment';
import { Account, LoginResponse, Register } from '../interfaces/auth.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl: string = environment.url;
  private _user!: Account;

  get user() {
    return { ...this._user };
  }

  setUserAmount(amount:number){
    const newBalance = (this._user.balance - amount);
    this._user = { ...this.user, balance: newBalance};
  }

  constructor(private httpClient: HttpClient) { }

  login(rut: string, password: string) {

    const url = `${this.baseUrl}/auth/login`;
    const body = { rut, password };

    return this.httpClient.post<LoginResponse>(url, body)
      .pipe(
        tap(resp => {
          if (resp.ok) {
            localStorage.setItem('token', resp.token!);
            this._user = resp.account;
          }
        }),
        map(resp => resp.ok),
        catchError(err => of(err.error))
      );
  }

  register(account:Register) {
    const url = `${this.baseUrl}/auth/register`;

    return this.httpClient.post<LoginResponse>(url, {...account})
      .pipe(
        tap(resp => {
          if (resp.ok) {
            localStorage.setItem('token', resp.token!);
            this._user = resp.account;
          }
        }),
        map(resp => resp.ok),
        catchError(err => of(err.error))
      );
  }

  validarToken(): Observable<boolean> {

    const url = `${this.baseUrl}/auth/renew`;
    const headers = new HttpHeaders()
      .set('x-token', localStorage.getItem('token') || '');

    return this.httpClient.get<LoginResponse>(url, { headers })
      .pipe(
        map(resp => {
          localStorage.setItem('token', resp.token!);
          this._user = resp.account;
          return resp.ok;
        }),
        catchError(err => of(false))
      );

  }

  logout() {
    localStorage.clear();
  }
}
