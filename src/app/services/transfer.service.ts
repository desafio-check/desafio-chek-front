import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of } from 'rxjs';

import { environment } from '../../environments/environment';
import { MakeTransfer } from '../interfaces/transfer.interface';
import { getHeaderWithToken } from './utils';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  private baseUrl: string = environment.url;

  constructor(private httpClient: HttpClient) { }


  makeTransfer(transfer:MakeTransfer){
    const url = `${this.baseUrl}/transfer/makeTransfer`;

    return this.httpClient.post(url, {...transfer}, {headers: getHeaderWithToken()})
      .pipe(
        catchError(err => of(err.error))
      );
  }

  getHistory() {
    const url = `${this.baseUrl}/transfer/getTransfers`;

    return this.httpClient.get(url, {headers: getHeaderWithToken()})
      .pipe(
        catchError(err => of(err.error))
      );
  }

}
