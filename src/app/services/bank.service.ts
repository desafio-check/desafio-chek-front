import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';

import { BanksResp } from '../interfaces/banks.interface';
import { getHeaderWithToken } from './utils';

@Injectable({
  providedIn: 'root'
})
export class BankService {
  private baseUrl: string = environment.url;

  constructor(private httpClient: HttpClient) { }

  getBanksList(){
    const url = `${this.baseUrl}/banks/getBanks`;

    return this.httpClient.get<BanksResp>(url, {headers: getHeaderWithToken()})
      .pipe(
        catchError(err => of(err.error))
      );
  }
}
