import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { rutValidator } from 'src/app/utils/validateRut';
import { AuthService } from '../../services/auth.service';
import { getErrorAlert } from '../../utils/sweetsalert';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    rut: ['', [Validators.required, rutValidator()]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
  }

  login() {
    if (this.loginForm.valid) {
      this.authService.login(
        this.loginForm.get('rut')?.value,
        this.loginForm.get('password')?.value
      )
      .subscribe((resp) => resp === true ?
          this.router.navigateByUrl('/home/addressee')
          : getErrorAlert(resp.msg)
      );
    }
  }

  onEnter() {
    this.login();
  }

  toRegister(){
    this.router.navigateByUrl('/auth/register');
  }
}