import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';
import { rutValidator } from 'src/app/utils/validateRut';
import { getErrorAlert } from '../../utils/sweetsalert';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = this.fb.group({
    rut: ['', [Validators.required, rutValidator()]],
    name: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  register() {
    if (this.registerForm.valid) {
      this.authService.register(this.getFormValues())
        .subscribe((resp) => resp === true ?
           this.router.navigateByUrl('/home/addressee')
           : getErrorAlert(resp.msg)
        );
    }else {
      this.registerForm.markAllAsTouched();
    }
  }

  getFormValues() {
    return {
      name: this.registerForm.get('name')?.value,
      rut: this.registerForm.get('rut')?.value,
      email: this.registerForm.get('email')?.value,
      phoneNumber: this.registerForm.get('phone')?.value,
      password: this.registerForm.get('password')?.value,
    }
  }

  toLogin() {
    this.router.navigateByUrl('/auth/login');
  }

  getErrorsForm(field: string){
    return this.registerForm.get(field)?.hasError('required') &&
    this.registerForm.get(field)?.touched
  }

  getErrorFormatRut(){
    return this.registerForm.get('rut')?.hasError('invalidRut') && this.registerForm.get('rut')?.touched
  }
}
