export interface BanksResp {
    ok:   boolean;
    resp: Resp;
}

export interface Resp {
    banks: Bank[];
}

export interface Bank {
    name: string;
    id:   string;
}