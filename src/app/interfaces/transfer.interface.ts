export interface MakeTransfer {
    mount:        number;
    bank:         string;
    rutAccount:   string;
    rutAddressee: string;
}

export interface TransferHistory {
    ok:        boolean;
    transfers: Transfer[];
}

export interface Transfer {
    mount:     number;
    addressee: Addressee;
    createdAt: Date;
    uid:       string;
}

export interface Addressee {
    _id:         string;
    name:        string;
    rut:         string;
    accountType: string;
    bank:        string;
}

export interface TransferMatData {
    nameAddressee: string;
    rut:           string;
    bank:         string;
    accountType:   string;
    amount:        number;
}
