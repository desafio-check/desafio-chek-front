export interface AddAddressee {
    uid:       string;
    addressee: Addressee;
}

export interface GetAddressee {
    ok:        boolean;
    addressee: Addressee;
}

export interface Addressee {
    name:          string;
    rut:           string;
    email:         string;
    accountType:   string;
    bank:          string;
    accountNumber?: number;
    phoneNumber:   string;
    uid?:           string;
}
