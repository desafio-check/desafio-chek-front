export interface LoginResponse {
    ok:      boolean;
    account: Account;
    token:   string;
}

export interface Account {
    name:          string;
    rut:           string;
    email:         string;
    balance:       number;
    accounType:    string;
    bank:          string;
    accountNumber: number;
}

export interface Register {
    name:     string;
    rut:      string;
    email:    string;
    password: string;
}