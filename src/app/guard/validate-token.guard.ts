import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ValidateTokenGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
  ){}

  canActivate(): Observable<boolean> | boolean{
    return this.authService.validarToken()
      .pipe(
        tap(ok => {
          if (!ok) {
            this.router.navigateByUrl('/auth/login');
          }
        })
      );
  }
  
}
