import Swal from 'sweetalert2';

export const getSuccesAlert = (msg: string) => {
  Swal.fire({
    title: 'Todo Bien!',
    text: msg,
    icon: 'success',
    confirmButtonText: 'Aceptar',
    confirmButtonColor: '#70578b',
  });
}

export const getErrorAlert = (msg: string) => {
  Swal.fire({
    title: 'Error!',
    text: msg,
    icon: 'error',
    confirmButtonText: 'Aceptar',
    confirmButtonColor: '#70578b',
  });
}

export const getConfirmAlert = (msg:string) => {
  return Swal.fire({
    text: msg,
    icon: 'question',
    confirmButtonText: 'Aceptar',
    confirmButtonColor: '#70578b',
    showDenyButton: true,
    denyButtonText: 'Cancelar',
  })
}