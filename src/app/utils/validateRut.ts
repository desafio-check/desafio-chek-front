import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function rutValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        return validateRut(control.value) ? { invalidRut: { value: control.value } } : null;
    };
}

//Devuelve falso si el digito verificador ingresado es distinto al que se calcula a partir del rut
const validateRut = (rut: string) => {
    if (!rut) return false;

    // const rutCompleto = rut.replace(/\./g, '');
    const regex = /^[d0-9]+[-|-]{1}[0-9kK]{1}$/;
    if (!regex.test(rut)) return true;

    const tmp = rut.split("-");
    return validateDv(parseInt(tmp[0])) !== tmp[1].toLowerCase();
}

const validateDv = (rut: number) => {
    let M = 0, S = 1;
    for (; rut; rut = Math.floor(rut / 10))
        S = (S + (rut % 10) * (9 - (M++ % 6))) % 11;

    return S ? (S - 1).toString() : 'k';
}