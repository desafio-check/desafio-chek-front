import { Component, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Output() changeOpened = new EventEmitter<boolean>();

  constructor(private auth: AuthService, private router: Router) { }

  get user() {
    return {
      name: this.auth.user.name,
      balance: this.auth.user.balance,
      photo: 'https://cdn-icons-png.flaticon.com/512/149/149071.png'
    };
  }

  signOut() {
    this.auth.logout();
    this.router.navigateByUrl('/auth/login');
  }

  changeOpenedValue() {
    this.changeOpened.emit();
  }
}
