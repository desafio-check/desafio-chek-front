import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { FormsModule } from '@angular/forms';

import { HomePanelComponent } from './home-panel/home-panel.component';
import { HomeRoutingModule } from './home-routing.module';
import { AddAddresseComponent } from './add-addresse/add-addresse.component';
import { TransferComponent } from './transfer/transfer.component';
import { RecordComponent } from './record/record.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [
    HomePanelComponent,
    AddAddresseComponent,
    TransferComponent,
    RecordComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    HomeRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
    FormsModule,
    MatPaginatorModule,
    MatTableModule,
  ]
})
export class HomeModule { }
