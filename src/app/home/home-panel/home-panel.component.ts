import { Component, HostListener, OnInit } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home-panel',
  templateUrl: './home-panel.component.html',
  styleUrls: ['./home-panel.component.scss']
})
export class HomePanelComponent implements OnInit {
  opened: boolean = false;
  menuMode: MatDrawerMode = 'side';

  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.onResize(window.innerWidth);
  }

  @HostListener('window:resize', ['$event.target.innerWidth'])
  onResize(width: number) {
    if (width < 768) {
      this.opened = false;
      this.menuMode = 'over';
    }else{
      this.opened = true;
      this.menuMode = 'side';
    }
  }

  signOut() {
    this.auth.logout();
    this.router.navigateByUrl('/auth/login');
  }

}
