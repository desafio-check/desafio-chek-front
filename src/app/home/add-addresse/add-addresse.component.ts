import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AddresseService } from '../../services/addressee.service';
import { Addressee } from '../../interfaces/addressee.interface';
import { BankService } from '../../services/bank.service';
import { Bank } from '../../interfaces/banks.interface';
import { getSuccesAlert } from 'src/app/utils/sweetsalert';
import { getErrorAlert } from '../../utils/sweetsalert';
import { rutValidator } from 'src/app/utils/validateRut';
@Component({
  selector: 'app-add-addresse',
  templateUrl: './add-addresse.component.html',
  styleUrls: ['./add-addresse.component.scss']
})
export class AddAddresseComponent implements OnInit {
  addresseeForm: FormGroup = this.fb.group({
    rut: ['', [Validators.required, rutValidator()]],
    name: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', [Validators.required]],
    bank: ['', [Validators.required]],
    accountType: ['',  [Validators.required]]
  });
  banks: Bank[]= [];
  accountType: string[] = ['Cuenta Vista', 'Cuenta Rut', 'Cuenta Corriente'];

  constructor(
    private fb: FormBuilder,
    private addresseeService: AddresseService,
    private bankService: BankService
  ) { }

  ngOnInit(): void {
    this.bankService.getBanksList()
      .subscribe((resp) => {
        if (resp.ok) {
          this.banks = resp.banks;
        }
      })
  }

  addAddressee(){
    if (this.addresseeForm.valid) {
      this.addresseeService.addAddresse(this.getFormValues())
        .subscribe(({ok, msg}) => {
          if(ok === true) {
            getSuccesAlert(msg);
            this.addresseeForm.reset();
          }else {
            getErrorAlert(msg);
          }
        });
    }else{
      getErrorAlert('Faltan campos por rellenar.');
      this.addresseeForm.markAllAsTouched();
    }
  }

  getFormValues(): Addressee{
    return {
      rut: this.addresseeForm.get('rut')?.value,
      name: this.addresseeForm.get('name')?.value,
      email: this.addresseeForm.get('email')?.value,
      phoneNumber: this.addresseeForm.get('phone')?.value,
      bank: this.addresseeForm.get('bank')?.value,
      accountType: this.addresseeForm.get('accountType')?.value
    }
  }

  getErrorsForm(field: string){
    return this.addresseeForm.get(field)?.hasError('required') &&
      this.addresseeForm.get(field)?.touched;
  }

  getErrorFormatRut(){
    return this.addresseeForm.get('rut')?.hasError('invalidRut') && 
      this.addresseeForm.get('rut')?.touched;
  }

}
