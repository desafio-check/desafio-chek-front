import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { format } from 'date-fns'

import { TransferService } from '../../services/transfer.service';
import { TransferHistory, TransferMatData } from 'src/app/interfaces/transfer.interface';

@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit {

  displayedColumns = ['nombreDestinatario', 'rut', 'banco', 'tipoCuenta', 'monto', 'fecha'];
  dataSource!: MatTableDataSource<TransferMatData>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private transferService: TransferService) { }

  ngOnInit(): void {
    this.transferService.getHistory()
      .subscribe((resp:TransferHistory) => {
        if(resp.ok){
          this.dataSource = new MatTableDataSource(resp.transfers.map((t) => ({
            nameAddressee: t.addressee.name,
            rut: t.addressee.rut,
            bank: t.addressee.bank,
            accountType: t.addressee.accountType,
            amount: t.mount,
            date: format(new Date(t.createdAt), 'dd-MM-yyyy')
          })) as TransferMatData[]);
          this.dataSource.paginator = this.paginator;
        }else{
          console.log(resp)
        }
      })
  }

}
