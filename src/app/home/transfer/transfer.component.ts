import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { Addressee } from 'src/app/interfaces/addressee.interface';
import { MakeTransfer } from 'src/app/interfaces/transfer.interface';
import { AuthService } from 'src/app/services/auth.service';
import { TransferService } from 'src/app/services/transfer.service';
import { rutValidator } from 'src/app/utils/validateRut';
import { AddresseService } from '../../services/addressee.service';
import { getErrorAlert, getSuccesAlert, getConfirmAlert } from '../../utils/sweetsalert';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {
  rut = new FormControl('', [Validators.required, rutValidator()]);
  amount = new FormControl(0, [Validators.required, Validators.min(1)]);
  addressee?: Addressee;

  constructor(
    private addresseeService: AddresseService,
    private transferService: TransferService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  getAddressee() {
    if (this.rut.valid) {
      this.addresseeService.getAddressee(this.rut.value!)
        .subscribe((resp) => {
          if (resp.ok) {
            this.addressee = resp.addressee;
          } else {
            getErrorAlert(resp.msg);
            this.addressee = {} as Addressee;
          }
        }
      );
    }
  }

  makeTransfer() {
    this.transferService.makeTransfer(this.getTransferData())
      .subscribe((resp) => {
        if (resp.ok) {
          getSuccesAlert(resp.msg);
          this.authService.setUserAmount(this.amount.value!);
          this.rut.reset();
          this.amount.reset();
          this.addressee = {} as Addressee;
        } else {
          getErrorAlert(resp.msg);
        }
      });
  }

  confirmTransfer() {
    if (this.addressee?.rut && this.amount.valid) {
      getConfirmAlert('¿Está seguro de que desea realizar la transacción?')
        .then((result) => {
          if (result.isConfirmed) {
            this.makeTransfer();
          }
        });
    } else {
      this.rut.markAsTouched();
      this.amount.markAsTouched();
      getErrorAlert('Revice que los datos del monto y destinatario sean correctos.');
    }
  }

  getTransferData(): MakeTransfer {
    return {
      mount: this.amount.value!,
      bank: this.addressee?.bank || '',
      rutAccount: this.authService.user.rut,
      rutAddressee: this.addressee?.rut || ''
    }
  }

  validateRequiredRut() {
    return this.rut.hasError('required') &&
      this.rut.touched;
  }

  validateFormatRut() {
    return this.rut.hasError('invalidRut') &&
      this.rut.touched;;
  }

}
