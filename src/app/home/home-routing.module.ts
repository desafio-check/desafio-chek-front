import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePanelComponent } from './home-panel/home-panel.component';
import { AddAddresseComponent } from './add-addresse/add-addresse.component';
import { TransferComponent } from './transfer/transfer.component';
import { RecordComponent } from './record/record.component';

const routes: Routes = [
  {
    path: '',
    component: HomePanelComponent,
    children: [
      {path:'addressee', component: AddAddresseComponent},
      {path:'transfer', component: TransferComponent},
      {path:'record', component: RecordComponent},
    ]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
